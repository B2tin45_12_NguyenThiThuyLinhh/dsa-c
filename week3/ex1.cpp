#include <iostream>
#include <string>
using namespace std;
struct MySNode
{
    int element;
    MySNode *next;
    MySNode(int e,MySNode *n)
    {
        element=e;
        next=n;
    }
};
class MySList
{
public:
    MySNode *head;
    long size,maxsize;
    MySList(int maxsize)
    {
        size=0;
        this->maxsize=maxsize;
        head=NULL;
    }
    bool isEmpty()
    {
        return (size==0);
    }
    bool isFull()
    {
        return (size==maxsize);
    }
    void Push(int e)
    {
        MySNode *v=new MySNode(e,head);
        head=v;
        size++;
    }
    int Pop()
    {
        if (!isEmpty())
        {
            MySNode *old=head;
            head=old->next;
            int s=old->element;
            delete old;
            size--;
            return s;
        }
    }
    int Top()
    {
        if (!isEmpty()) return (head->element);
    }
    void Tra(MySNode *node)
    {
        if (node==NULL) return;
        cout<<node->element<<" ";
        Tra(node->next);
    }
    void Print()
    {
        Tra(head);
        cout<<endl;
    }
};
struct MyDNode
{
    string element;
    MyDNode *prev,*next;
    MyDNode(string e,MyDNode *p,MyDNode *n)
    {
        element=e;
        prev=p;
        next=n;
    }
};
class MyDList
{
public:
    MyDNode *header,*trailer;
    long size;
    MyDList()
    {
        size=0;
        header=new MyDNode("",NULL,NULL);
        trailer=new MyDNode("",header,NULL);
        header->next=trailer;
    }
    bool isEmpty()
    {
        return (size==0);
    }
    string front()
    {
        if (!isEmpty()) return (header->next->element);
    }
    void add(MyDNode *n,string e)
    {
        MyDNode *u=new MyDNode(e,n->prev,n);
        n->prev->next=u;
        n->prev=u;
        size++;
    }
    void Push(string e)
    {
        add(trailer,e);
    }
    void remove(MyDNode *n)
    {
        MyDNode *u=n->prev;
        MyDNode *v=n->next;
        u->next=v;
        v->prev=u;
        delete n;
        size--;
    }
    void Pop()
    {
        remove(header->next);
    }
    void traverse(MyDNode *node)
    {
        node=node->next;
        if (node==trailer) return;
        cout<<node->element<<" ";
        traverse(node);
    }
};
int main()
{
    MySList msl(10);
    msl.Push(1);
    msl.Push(2);
    cout<<msl.Top()<<endl;
    msl.Print();
    cout<<msl.Pop()<<endl;
    msl.Print();
    MyDList mdl;
    mdl.Push("a");
    mdl.Push("b");
    mdl.Push("c");
    mdl.traverse(mdl.header);
    cout<<endl;
    mdl.Pop();
    mdl.traverse(mdl.header);
    cout<<endl;
    mdl.Pop();
    mdl.traverse(mdl.header);
}
