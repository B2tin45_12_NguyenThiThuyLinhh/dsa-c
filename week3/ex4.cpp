#include <iostream>
#include <string>
using namespace std;
struct MyDNode
{
    string element;
    MyDNode *prev,*next;
    MyDNode(string e,MyDNode *p,MyDNode *n)
    {
        element=e;
        prev=p;
        next=n;
    }
};
class MyDList
{
public:
    MyDNode *head,*tail;
    long size;
    MyDList()
    {
        size=0;
        head=new MyDNode("",NULL,NULL);
        tail=new MyDNode("",head,NULL);
        head->next=tail;
    }
    bool isEmpty()
    {
        return (size==0);
    }
    string front()
    {
        if (!isEmpty()) return (head->next->element);
    }
    string back()
    {
        if (!isEmpty()) return (tail->prev->element);
    }
    void add(MyDNode *n,string e)
    {
        MyDNode *u=new MyDNode(e,n->prev,n);
        n->prev->next=u;
        n->prev=u;
        size++;
    }
    void addFront(string e)
    {
        add(head->next,e);
    }
    void addBack(string e)
    {
        add(tail,e);
    }
    void remove(MyDNode *n)
    {
        MyDNode *u=n->prev;
        MyDNode *v=n->next;
        u->next=v;
        v->prev=u;
        delete n;
        size--;
    }
    void removeFront()
    {
        remove(head->next);
    }
    void removeBack()
    {
        remove(tail->prev);
    }
    void traverse(MyDNode *node)
    {
        node=node->next;
        if (node==tail) return;
        cout<<node->element<<" ";
        traverse(node);
    }
    void Connect(MyDNode *head1,MyDNode *tail1,MyDNode *head2,MyDNode *tail2)
    {
        tail1->prev->next=head2->next;
        head2->next->prev=tail1->prev;
        head=head1;
        tail=tail2;
    }
};
int main()
{
    MyDList md1;
    MyDList md2;
    md1.addFront("1");
    md1.addBack("2");
    md1.addBack("3");
    md2.addFront("4");
    md2.addFront("5");
    md2.addBack("6");
    md1.traverse(md1.head);
    cout<<endl;
    md2.traverse(md2.head);
    cout<<endl;
    //Nối 2 danh sách liên kết kép md1 và md2, sau đó lưu vào md1
    md1.Connect(md1.head,md1.tail,md2.head,md2.tail);
    md1.traverse(md1.head);
}
