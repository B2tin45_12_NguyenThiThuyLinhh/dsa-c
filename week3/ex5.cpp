#include <iostream>
#include <string>
using namespace std;
struct MySNode
{
    string element;
    MySNode *next;
    MySNode(string e,MySNode *n)
    {
        element=e;
        next=n;
    }
};
class MySList
{
public:
    MySNode *head;
    long size;
    MySList()
    {
        size=0;
        head=NULL;
    }
    bool isEmpty()
    {
        return (size==0);
    }
    string front()
    {
        if (!isEmpty()) return (head->element);
    }
    void addFront(string e)
    {
        MySNode *v=new MySNode(e,head);
        head=v;
        size++;
    }
    string removeFront()
    {
        if (!isEmpty())
        {
            MySNode *old=head;
            head=old->next;
            string s=old->element;
            delete old;
            size--;
            return s;
        }
    }
    void traverse(MySNode *node)
    {
        if (node==NULL) return;
        cout<<node->element<<" ";
        traverse(node->next);
    }
    void Lienket(string a[], int t, MySNode *node)
    {
        if (node==NULL) return;
        a[t]=node->element;
        t++;
        Lienket(a,t,node->next);
    }
    void kolienket(string a[],int t,MySNode *node)
    {
        if (node==NULL) return;
        node->element=a[t];
        t++;
        kolienket(a,t,node->next);
    }
    void SW(string a[],int i, int j)
    {
        if (i>=j) return;
        swap(a[i],a[j]);
        SW(a,i+1,j-1);
    }
    void Swap()
    {
        string *a;
        a=new string[size];
        Lienket(a,0,head);
        SW(a,0,size-1);
        kolienket(a,0,head);
    }
};
int main()
{
    MySList ms;
    cout<<"ban dau: ";
    ms.addFront("1");
    ms.addFront("2");
    ms.addFront("3");
    ms.addFront("4");
    ms.addFront("5");
    ms.traverse(ms.head);
    cout<<endl;
    cout<<"dao nguoc: ";
    ms.Swap();
    ms.traverse(ms.head);
}
