#include <iostream>
#include <string>
using namespace std;
struct MySNode
{
    string element;
    MySNode *next;
    MySNode(string e,MySNode *n)
    {
        element=e;
        next=n;
    }
};
class MySList
{
public:
    MySNode *head;
    MySList()
    {
        head=NULL;
    }
    bool isEmpty()
    {
        return (head==NULL);
    }
    string front()
    {
        if (!isEmpty()) return (head->element);
    }
    void addFront(string e)
    {
        MySNode *v=new MySNode(e,head);
        head=v;
    }
    string removeFront()
    {
        if (!isEmpty())
        {
            MySNode *old=head;
            head=old->next;
            string s=old->element;
            delete old;
            return s;
        }
    }
    void traverse(MySNode *node)
    {
        if (node==NULL) return;
        cout<<node->element<<endl;
        traverse(node->next);
    }
    void Cout(int &t, MySNode *node)
    {
        if (node == NULL) return;
        t++;
        Cout(t,node->next);
    }
    //Hàm tính kích thước danh sách liên kết đơn
    int Size()
    {
        int t=0;
        Cout(t,head);
        return t;
    }
};
int main()
{
    MySList ms;
    ms.addFront("hoc");
    ms.addFront("vien");
    ms.addFront("an");
    ms.addFront("ninh");
    ms.addFront("nhan");
    ms.addFront("dan");
    ms.traverse(ms.head);
    cout<<"size="<<ms.Size()<<endl;
    //cout<<ms.front()<<endl;
    if (!ms.isEmpty()) ms.removeFront();
    ms.traverse(ms.head);
}
