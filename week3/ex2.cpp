#include <iostream>
using namespace std;
class MyCListQueue
{
public:
    int *q;
    long maxsize;
    long size, f, r;
    MyCListQueue(int maxsize)
    {
        this->maxsize=maxsize;
        q=new int[maxsize+1];
        size=0;
        f=1;
        r=0;
    }
    bool isEmpty()
    {
        return (size==0);
    }
    bool isFull()
    {
        return (size==maxsize);
    }
    void enQueue(int n)
    {
        if (!isFull())
        {
            size++;
            r=r%maxsize+1;
            q[r]=n;
        }
        else cout<<"Full"<<endl;
    }
    int deQueue()
    {
        if (!isEmpty())
        {
            size--;
            int n=q[f];
            f=f%maxsize+1;
            return n;
        }
        else
        {
            cout<<"Empty"<<endl;
            return 0;
        }
    }
    int front()
    {
        return q[f];
    }
    void Print()
    {
        for (int i=1;i<=size;i++)
            cout<<q[(i+f-2)%maxsize+1]<<" ";
        cout<<endl;
    }
};
int main()
{
    MyCListQueue my(3);
    cout<<my.maxsize<<endl;
    my.enQueue(1);
    my.enQueue(2);
    cout<<my.size<<endl;
    my.Print();
    cout<<my.deQueue()<<endl;
    my.Print();
    my.enQueue(3);
    my.enQueue(4);
    my.enQueue(5);
    my.Print();
    cout<<my.deQueue()<<endl;
    cout<<my.deQueue()<<endl;
    my.enQueue(10);
    my.Print();
    cout<<my.front();
}
