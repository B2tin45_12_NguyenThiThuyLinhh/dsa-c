#include <iostream>
#include <vector>
#include <stdio.h>
#include <cmath>
using namespace std;
struct DataElement
{
    string data;
    DataElement(string data)
    {
        this->data=data;
    }
};

struct Node
{
    DataElement *e;
    Node *parent, *left, *right;
    Node(DataElement *e, Node *parent, Node *left, Node *right)
    {
        this->e=e;
        this->parent=parent;
        this->left=left;
        this->right=right;
    }
    bool isLeaf()
    {
        return (left==NULL && right==NULL);
    }
    bool isRoot()
    {
        return parent==NULL;
    }
};

class Tree
{
public:
    Node *root;
    vector <Node*> ptreNodes;
    long size;
    double result;
    bool isEmpty()
    {
        return size==0;
    }

    bool isNumber(string s)
    {
        return (s[0]>='0' && s[0]<='9');
    }

    double toNumber(string c)
    {
        double r=0;
        for (int i=0;i<c.length();i++)
            r=r*10+(c[i]-'0');
        return r;
    }

    //Tính giá trị biểu thức
    void Cout()
    {
        double a[ptreNodes.size()];
        int d=-1;
        double u,v;
        result=0;
        for (int i=0;i<ptreNodes.size();i++)
            if (isNumber(ptreNodes[i]->e->data))
            {
                d++;
                a[d]=toNumber(ptreNodes[i]->e->data);
            }
            else
            {
                v=a[d];
                d--;
                u=a[d];
                switch (ptreNodes[i]->e->data[0])
                {
                case '+':
                    {
                        a[d]=u+v;
                        break;
                    }
                case '-':
                    {
                        a[d]=u-v;
                        break;
                    }
                case '*':
                    {
                        a[d]=u*v;
                        break;
                    }
                case '/':
                    {
                        a[d]=u/v;
                        break;
                    }
                }
            }
        result=a[0];
    }

    void Built()
    {
        int a[ptreNodes.size()];
        int d=-1,u,v;
        for (int i=0;i<ptreNodes.size();i++)
            if (isNumber(ptreNodes[i]->e->data))
            {
                d++;
                a[d]=i;
            }
            else
            {
                v=a[d];
                d--;
                u=a[d];
                a[d]=i;
                ptreNodes[u]->parent=ptreNodes[i];
                ptreNodes[v]->parent=ptreNodes[i];
                ptreNodes[i]->left=ptreNodes[u];
                ptreNodes[i]->right=ptreNodes[v];
            }
    }

    void buildTree(char *fileName)
    {
        freopen(fileName,"rt",stdin);
        string s;
        getline(cin,s);
        s=s+' ';
        string c="";
        for (int i=0;i<s.length();i++)
        {
            if (s[i]==' ')
            {
                ptreNodes.push_back(new Node(new DataElement(c),NULL,NULL,NULL));
                c="";
            }
            else
                c=c+s[i];
        }
        root=ptreNodes[ptreNodes.size()-1];
        Cout();
        Built();
    }

    void inOrder(Node *p)
    {
        if (p->isLeaf())
        {
            cout<<p->e->data<<" ";
            return;
        }
        inOrder(p->left);
        cout<<p->e->data<<" ";
        inOrder(p->right);
    }
};

int main()
{
    Tree tr;
    tr.buildTree("caybieuthuc.input");
    freopen("caybieuthuc.output","wt",stdout);
    tr.inOrder(tr.root);
    cout<<endl;
    cout<<tr.result;
}
