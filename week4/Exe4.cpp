#include <iostream>
#include <stdio.h>
#include <cmath>
using namespace std;
struct DataElement
{
    char data;
    DataElement(char data)
    {
        this->data=data;
    }
};

struct Node
{
    DataElement *e;
    Node *parent;
    Node *left;
    Node *right;
    Node(DataElement *e,Node *parent,Node *left,Node *right)
    {
        this->e=e;
        this->parent=parent;
        this->left=left;
        this->right=right;
    }
};

class BTree
{
public:
    Node *root;
    long size,height;
    BTree()
    {
        size=0;
    }

    bool isEmpty()
    {
        return (size==0);
    }

    bool isLeaf(Node *p)
    {
        return (p->left==NULL && p->right==NULL);
    }

    bool isRoot(Node *p)
    {
        p->parent==NULL;
    }

    long getHeight(Node *p)
    {
        if (isLeaf(p)) return 1;
        long l=0,r=0;
        if (p->left!=NULL) l=getHeight(p->left);
        if (p->right!=NULL) r=getHeight(p->right);
        return max(l,r)+1;
    }

    long getDepth(Node *p)
    {
        if (p==root) return 0;
        return (getDepth(p->parent)+1);
    }

    Node* addRoot(DataElement *e)
    {
        Node *p = new Node(e,NULL,NULL,NULL);
        root=p;
        height=1;
        size=1;
    }

    Node* expandExternal(Node *p,DataElement *e,bool left)
    {
        Node *child=new Node(e,p,NULL,NULL);
        if (left)
            p->left=child;
        else p->right=child;
        height=getHeight(root);
        size++;
        return child;
    }

    void preOrder(Node *p)
    {
        cout<<p->e->data;
        if (isLeaf(p)) return;
        if (p->left!=NULL) preOrder(p->left);
        if (p->right!=NULL) preOrder(p->right);
    }

    void inOrder(Node *p)
    {
        if (isLeaf(p))
        {
            cout<<p->e->data;
            return;
        }
        if (p->left!=NULL)
        {
            inOrder(p->left);
            cout<<p->e->data;
            if (p->right!=NULL) inOrder(p->right);
        }
        else
        {
            inOrder(p->right);
            cout<<p->e->data;
        }
    }

    void postOrder(Node *p)
    {
        if (p->left!=NULL) postOrder(p->left);
        if (p->right!=NULL) postOrder(p->right);
        cout<<p->e->data;
    }
};

int main()
{
    BTree tr;
    tr.addRoot(new DataElement('A'));
    Node *pl,*pr;
    pl=tr.expandExternal(tr.root,new DataElement('B'),true);
    pl=tr.expandExternal(pl,new DataElement('D'),false);
    pl=tr.expandExternal(pl,new DataElement('K'),true);
    pr=tr.expandExternal(tr.root,new DataElement('C'),false);
    pl=tr.expandExternal(pr,new DataElement('E'),true);
    pr=tr.expandExternal(pr,new DataElement('F'),false);
    pr=tr.expandExternal(pl,new DataElement('G'),false);
    cout<<"Size: "<<tr.size<<endl;
    cout<<"Height: "<<tr.getHeight(tr.root)<<endl;
    cout<<"Depth: "<<tr.getDepth(tr.root)<<endl;
    cout<<"PreOrder: ";
    tr.preOrder(tr.root);
    cout<<endl;
    cout<<"PostOrder: ";
    tr.postOrder(tr.root);
    cout<<endl;
    cout<<"InOrder: ";
    tr.inOrder(tr.root);
}
