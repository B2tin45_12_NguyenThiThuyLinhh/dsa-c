#include <iostream>
#include <vector>
#include <stdio.h>
#include <cmath>
using namespace std;
struct DataElement
{
    char data;
    DataElement(char data)
    {
        this->data=data;
    }
};

struct Node
{
    DataElement *e;
    Node *parent;
    vector<Node*> children;
    Node(DataElement *e)
    {
        this->e=e;
    }
    bool isLeaf()
    {
        return (children.size()==0);
    }
    bool isRoot()
    {
        return parent==NULL;
    }
};

class Tree
{
public:
    Node *root;
    vector <Node*> ptreNodes;
    long size,height;
    bool isEmpty()
    {
        return size==0;
    }

    long getHeight(Node *p)
    {
        if (p->isLeaf()) return 1;
        long maxH=0;
        for (int i=0;i<p->children.size();i++)
        {
            maxH=max(getHeight(p->children[i]),maxH);
        }
        return maxH+1;
    }

    long getDepth(Node *p)
    {
        if (p==root) return 0;
        return (getDepth(p->parent)+1);
    }

    void buildTree(char *fileName)
    {
        freopen(fileName,"rt",stdin);
        string s;
        getline(cin,s);
        size=s.length();
        for (int i=1;i<=s.length();i++)
        {
            DataElement *e=new DataElement(s[i-1]);
            Node *node=new Node(e);
            ptreNodes.push_back(node);
        }
        for (int i=1;i<=s.length();i++)
        {
            int u,v;
            cin>>u;
            for (int j=1;j<=u;j++)
            {
                cin>>v;
                ptreNodes[v-1]->parent=ptreNodes[i-1];
                ptreNodes[i-1]->children.push_back(ptreNodes[v-1]);
            }
        }
        root=ptreNodes[0];
        height=getHeight(root);
    }

    void preOrder(Node *p)
    {
        cout<<p->e->data;
        if (p->isLeaf()) return;
        for (int i=0;i<p->children.size();i++)
            preOrder(p->children[i]);
    }

    void inOrder(Node *p)
    {
        if (p->isLeaf())
        {
            cout<<p->e->data;
            return;
        }
        inOrder(p->children[0]);
        cout<<p->e->data;
        for (int i=1;i<p->children.size();i++)
            inOrder(p->children[i]);
    }

    void postOrder(Node *p)
    {
        for (int i=0;i<p->children.size();i++)
            postOrder(p->children[i]);
        cout<<p->e->data;
    }
};

int main()
{
    Tree tr;
    tr.buildTree("treel.input");
    freopen("treel.output","wt",stdout);
    cout<<tr.getHeight(tr.ptreNodes[2])<<endl;
    cout<<"Height: "<<tr.height<<endl;
    cout<<"PreOrder: ";
    tr.preOrder(tr.root);
    cout<<endl;
    cout<<"InOrder: ";
    tr.inOrder(tr.root);
    cout<<endl;
    cout<<"PostOrder: ";
    tr.postOrder(tr.root);
    cout<<endl;
}
