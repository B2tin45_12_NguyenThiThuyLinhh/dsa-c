#include <iostream>
#include <stdio.h>
#include <vector>
#include <cmath>
using namespace std;
const long CAPACITY = 100;
class Tree
{
public:
    char node[CAPACITY];
    vector<int> arrOfChildrenList[CAPACITY];
    int arrOfParent[CAPACITY];
    long size,height;

    bool isLeaf(int pos)
    {
        return (arrOfChildrenList[pos].size()<=0);
    }

    bool isRoot(int pos)
    {
        return (arrOfParent[pos]==0);
    }

    long getHeight(int pos)
    {
        if (isLeaf(pos)) return 1;
        long maxH=0;
        for (int i=0;i<arrOfChildrenList[pos].size();i++)
            maxH=max(maxH,getHeight(arrOfChildrenList[pos][i]));
        return (maxH+1);
    }

    long getDepth(int pos)
    {
        if (isRoot(pos)) return 0;
        return (getDepth(arrOfParent[pos])+1);
    }

    vector<int> getChildren(int pos)
    {
        return arrOfChildrenList[pos];
    }

    int getParent(int pos)
    {
        return arrOfParent[pos];
    }

    void buildTree( char *fileName)
    {
        freopen(fileName,"rt",stdin);
        string s;
        getline(cin,s);
        size=s.length();
        for (int i=1;i<=size;i++)
        {
            node[i]=s[i-1];
            arrOfParent[i]=0;
        }
        for (int i=1;i<=size;i++)
        {
            int u,v;//u la so luong parent
            cin>>u; //v la so luong children cua parent
            for (int j=1;j<=u;j++)
            {
                cin>>v;
                arrOfChildrenList[i].push_back(v);
                arrOfParent[v]=i;
            }
        }
        for (int i=1;i<=size;i++)
            if (isRoot(i)) height=getHeight(i);
    }

    void preOrder(int pos)
    {
        cout<<node[pos];
        if (isLeaf(pos)) return;
        for (int i=0;i<arrOfChildrenList[pos].size();i++)
        {
            preOrder(arrOfChildrenList[pos][i]);
        }
    }

    void postOrder(int pos)
    {
        for (int i=0;i<arrOfChildrenList[pos].size();i++)
            postOrder(arrOfChildrenList[pos][i]);
        cout<<node[pos];
    }

    void inOrder(int pos)
    {
        if (isLeaf(pos))
        {
            cout<<node[pos];
            return;
        }
        inOrder(arrOfChildrenList[pos][0]);
        cout<<node[pos];
        for (int i=1;i<arrOfChildrenList[pos].size();i++)
        {
            inOrder(arrOfChildrenList[pos][i]);
        }
    }
};

int main()
{
    Tree tr;
    tr.buildTree("treel.input");
    freopen("treel.output","wt",stdout);
    cout<<tr.getHeight(3)<<endl;
    cout<<"Height: "<<tr.height<<endl;
    cout<<"PreOrder: ";
    tr.preOrder(1);
    cout<<endl;
    cout<<"InOrder: ";
    tr.inOrder(1);
    cout<<endl;
    cout<<"PostOrder: ";
    tr.postOrder(1);
    cout<<endl;
}
